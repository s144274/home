# Introduction to the course 02148: Introduction to Coordination in Distributed Applications

This course is about coordinating distributed applications. Here are some videos of projects done in last editions of the course:
* [Bomberbois game](https://youtu.be/3BjpuX5mUxo)
* [Medieval resource game](https://youtu.be/r_5r8lhARJE)
  
## What is coordination?

According to the Oxford English Dictionary "coordination" is "the organization of the different elements of a complex body or activity so as to enable them to work together effectively". In IT systems the "elements" are computational entities (programs/applications/services/processes/etc.) and the "complex body or activity" is a concurrent system that emerges by making the individual entities work together, either cooperating or competing. In our course coordination is about organising the interaction between software applications possibly running on different devices, for example to build a complex distributed application.

## What do I need to follow this course?
You need some familiarity with programming in a language like Java, i.e. some ability to design and implement algorithms and data structures, and some ability to project a software application (see prerequisites for DTU students).


## What will I learn?

You will learn a simple yet powerful model of coordination, and you will use it to model and develop frameworks and applications for distributed computing.

## Why distributed tuple spaces?

Tuple spaces are easy to understand and provide simple model of coordination for distributed applications. They also provide a good basis to learn other models and frameworks for coordination such as message-passing libraries, actor-based languages, web services, distributed database and storage systems, and parallel programmings based on shared memory.

## What will I be doing?
The main activity of the course is to develop a project in a team. The course includes lectures on distributed programming with tuple spaces. The lectures and the projects will be based on [pSpaces](https://github.com/pSpaces), a set of libraries for Java, C#, Go, Swift and JavaScript.

## Which kind of projects can I do?

Projects can be of two types: (1) Use [pSpaces](https://github.com/pSpaces) to implement a distributed application or (2) Enrich [pSpaces](https://github.com/pSpaces) with new libraries, features or tools.

## Which projects have been done in the past?

Examples of projects in previous editions of the course include: tuple space frameworks for .NET, Python and Raspberry Pis, multiplayer turn-based and real-time games, simulators of drones and robots, home automation systems, resource and stock management systems, scheduling and planning systems, image processing applications, web-based collaborative tools, and sensor coordination systems.

## Is this useful at all in real life?

Yes, coordination is present everywhere, in web services and applications, in parallel and high-performance computing, in communication protocols of all kinds, in embedded and cyber-physical systems, in intelligent and multi-agent systems. We believe that the abilities you will acquire in this course will be useful whenever you will study such systems and, possibly, develop such kinds of systems.

# Where do we meet?

Room: B324/060.

# What is the plan?

See calendar below. Red events are meetings (we meet in class), green events group work, i.e. free time your students to work on their own (no need to come to class).
Both calendars are publicly available:
Meetings
Group work

Both calendars are publicly available:
 * [Meeetings](https://calendar.google.com/calendar/ical/l7eis3l6vg7t4vicnu0ju10id8%40group.calendar.google.com/private-6936d0b60d3f4d810725fe48c6bd83db/basic.ics)
 * [Group work](https://calendar.google.com/calendar/ical/k1e82qdu7f9j578pean95oh4cg%40group.calendar.google.com/public/basic.ics)

<iframe src="https://calendar.google.com/calendar/embed?title=02148%20-%20Introduction%20to%20coordination&amp;mode=AGENDA&amp;height=600&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=k1e82qdu7f9j578pean95oh4cg%40group.calendar.google.com&amp;color=%232F6309&amp;src=l7eis3l6vg7t4vicnu0ju10id8%40group.calendar.google.com&amp;color=%23B1440E&amp;ctz=Europe%2FCopenhagen" style="border-width:0" width="800" height="600" frameborder="0" scrolling="no"></iframe>


# More information?

* [Official course description](http://www.kurser.dtu.dk/02148.aspx)
* [CampusNet group](https://www.campusnet.dtu.dk/)
* Teacher: [Alberto Lluch Lafuente](http://www.imm.dtu.dk/~albl/), Building 324, Room 180 Ph. (+45) 4525 7509 , albl@dtu.dk 