Welcome to DTU course 02148 (Introduction to Coordination in Distributed Applications).

Here you will find:
 * [Course information](presentation.md)
 * [Lecture plan](schedule.md)
 * [Exercises](exercises.md)