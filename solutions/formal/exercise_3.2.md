```
board |-> "fork"*"fork"*"lock" |- 
  space.get("lock"); space.get("fork"); space.get("fork"); space.put("fork"); space.put("fork"); put("lock")
‖ space.get("lock"); space.get("fork"); space.get("fork"); space.put("fork"); space.put("fork"); put("lock")

⇓

board |-> "fork"*"fork" |- 
  space.get("fork"); space.get("fork"); space.put("fork"); space.put("fork"); put("lock")
‖ space.get("lock"); space.get("fork"); space.get("fork"); space.put("fork"); space.put("fork"); put("lock")

⇓

board |-> "fork" |- 
  space.get("fork"); space.put("fork"); space.put("fork").put("lock")
‖ space.get("lock"); space.get("fork")' space.get("fork"); space.put("fork"); space.put("fork"); put("lock")

⇓

board |-> nil |- 
  space.put("fork"); space.put("fork"); put("lock")
‖ space.get("lock"); space.get("fork"); space.get("fork"); space.put("fork"); space.put("fork").put("lock")

⇓

board |-> "fork" |- 
  space.put("fork"); put("lock")
‖ space.get("lock"); space.get("fork"); space.get("fork"); space.put("fork"); space.put("fork"); put("lock")

⇓

board |-> "fork"*"fork" |- 
  put("lock")
‖ space.get("lock").space.get("fork").space.get("fork").space.put("fork").space.put("fork").put("lock")

⇓

board |-> "fork"*"fork"*"lock" |- 
  space.get("lock"); space.get("fork"); space.get("fork"); space.put("fork"); space.put("fork"); space.put("lock")
  
⇓

board |-> "fork"*"fork" |- 
  space.get("fork"); space.get("fork"); space.put("fork"); space.put("fork"); space.put("lock")

⇓

board |-> "fork" |- 
  space.get("fork"); space.put("fork"); space.put("fork"); space.put("lock")

⇓

board |-> nil |- 
  space.put("fork"); space.put("fork"); space.put("lock")

⇓

board |-> "fork" |- 
  space.put("fork"); space.put("lock")

⇓

board |-> "fork"*"fork" |- 
  space.put("lock")

⇓

board |-> "fork"*"fork"*"lock" |- 
  0


```

