# 6. Coordination models

So far we have adopted a *process-oriented* approach to modelling and programming pSpace applications where applications are conceived as a set of processes (possibly running in different applications and hosts) that interact through tuple spaces. The focus is pretty much on programming the processes. 

The process-oriented approach is a *bottom-up* approach to programming distributed applications: the application logic emerges from the composition of the processes. Distributed applications can also be modelled using *top-down* approaches where the global application logic is defined first. The coordination logic of the processes is then synthetised from that global logic. One advantage of *top-down* approaches is that, under certain conditions, the obtained application enjoys good properties (e.g. deadlock-freedom) by design, so that no verification is needed.

This chapter will provide some common patterns of process-oriented approaches, and will present a set of bottom-up approaches, explaining how to adopt them in pSpaces. In particular, we will consider:
 * *process-oriented* programming patterns such as client-server, peer-to-peer, and *actors* systems.
 * *task-oriented* programming (e.g.. workflows) where we focus on tasks to be completed and the order among them.
 * *stream-oriented* programming (e.g. dataflows) where we focus on streams of data to be processesed by a network of data processing and routing units.

## 6.1 Process-oriented programming

### Client-server pattern
In the client-server pattern, we distinguish two kind of roles: *servers* (ready to offer some service) and *clients* (prompt to request services). Components with different roles tend to be asymmetric and exhibit differen behaviours, specialised for each role. 

In an application a component can sometimes adopt the role of a server or of a client. For instance, in an example we will see later on, a server hosting some service to users may need to request authentication services to an identity provider system.

Typically a server will interact with many clients, and a client with one or few servers. A server can thus become a *bottleneck* and a *single point-of-failure*. A possible way to mitigate this is to use server replication techniques.

Typical examples of client-server applications we have seen in the lectures are the chat applications where we had a server to host and display the chat rooms and one client for each chat user. One of the examples uses a simple server replication technique in which each chat room was managed by an ad-hoc server process.  


### Peer-to-peer pattern
In the peer-to-peer pattern all components of the application, called *peers*, tend to be *equal* in terms of the behaviour they exibit and their role within the application. In principle, there is some sort of symmetry among the peers. The coordination of peer-to-peer applications is distributed and fully *decentralised*: there is no need of a central coordinator. Sometimes, however, there is a need to break symmetry and decentralisation, for example to initiate the application or to lead part of the coordination logic. Peer-to-peer applications tend to be more robust than client-server applications since there is no single point of failure. On the other hand, peer-to-peer may have more security concerns as we need to trust all peers or adopt techniques to control and mitigate malicious behaviours. 

A typical example we have seen in the lectures are the dining philosophers: in that application all philosophers are peers running the same code and a central coordinator (the waiter) is only used to initiate the application.

### Actors

In this pattern, an application is conceived as a set of interacting *actors*. An actor is a computational entity with some internal state and with a special space called *inbox* that he uses to receive messages from other actors. The messages of an inbox can be read by the inbox's owner. An actor cannot send messages to its own inbox. The internal state of an actor is privated: actors can only interact through their inboxes. The general pattern for an actor is to react to the messages in the inbox, e.g. by changing their internal state, sending messages to other actors, or even create more actors. Actors can also have a proactive behaviour and they can be multi-threaded.

Actor-based programming is supported by mainstream languages such as [Scala](http://www.scala-lang.org/old/node/242) and [Erlang](http://erlang.org/doc/getting_started/conc_prog.html), and by actor libraries such as [Akka](https://akka.io/).


## 6.2 Task-oriented programming

In task-oriented programming we see applications as set of tasks to be executed according to some rules regulating the order of execution. Those rules impose control flow dependencies among tasks. For the sake of simplicity we consider here a simple task-oriented programming model in which tasks can be executed only once and the only kind of control flow dependency is of the form "task A has to be executed before task B".

More formally, we define a workflow as a set (N,E) such that
* N is a set of tasks
* E is a set of pairs of tasks
* The graph obtained by taking N as a set of nodes and E as a set of edges is acyclic.

Consider the following example:
* N = {Alice,Bob,Charlie,Dave}
* E = { (Alice,Bob), (Alice,Charlie), (Bob,Dave) , (Charlie,Dave) }

Which we can represent graphically as 
```
      /---> Bob -------\
     |                 |
     |                 v
 Alice                Dave
     |                 ^
     |                 |
      \---> Charlie ---/
```

Informally, the workflow says that Alice does not need to wait for anyone, Bob and Charlie need to wait for Alice to be done, and Dave needs to wait for both Bob and Charlie. As a result, the application will execute Alice first, then Bob and Charlie in any order or concurrently, and finally Dave.

A workflow can be implemented as a pSpace application in several ways. We present first a simple approach to generate a *concurrent* implementation and will later discuss how to generate a *distributed* implementation.

The idea to generate a concurrent implementation for a workflow (N,E) is as follows:
1. Create a space `rules` and put all control flow dependencies (i.e. all pairs in E) into `rules`.
2. Introduce rules (X,Main) for each activity X no other activity is waiting for. This will be used to detect termination of the workflow.
3. Create an empty space `tokens`.
4. For each task X in N build and launch a process Coordinate(X) that proceeds as follows:
 * read all control flow dependencies (\_,X) and (X,\_) from `rules`
 * for each control flow dependency (Y,X), wait to see token `(Y,X)` as a tuple in space `tokens`
 * execute task X
 * for each control flow dependency (X,Y), put token `(X,Y)` as a tuple in space `tokens`
5. For each rule (X,Main), wait token `(X,Main)`.

In Go, the code for a coordinator of tasks would look like this

```
func coordinateTask(task func(), me string, rules *Space, tokens *Space) {

	// Read order constraints
	var who string
	before, _ := rules.QueryAll(&who, me)
	after, _ := rules.QueryAll(me, &who)

	// Wait for tokens of previous tasks
	for _, edge := range before {
		who = (edge.GetFieldAt(0)).(string)
		tokens.Get(who, me)
	}

	// Execute task
	task()

	// Send tokens to tasks that come next
	for _, edge := range after {
		who = (edge.GetFieldAt(1)).(string)
		tokens.Put(me, who)
	}
}
```

The full code of the example can be found [here](https://github.com/pSpaces/goSpace-examples/blob/master/tutorial/workflow-0/main.go).

This approach to implement a workflow as a concurrent program can be extended to generate distributed implementation as well. All we would need to decide is where to locate the rules, tokens and task coordinators. In the extreme case in which each task coordinator needs to be run as a standalone programs (possibly in different hosts), a reasonable approach for distributing tokens and rules would be to place them in spaces close to the programs. For instance, each token `(X,Y)` could be assigned to a space hosted by the coordinator of `Y` (as a sort of inbox).

The approach sketched above can also be extended to deal with richer forms of task-oriented specificatios, for example workflows including standard control flow constructs such as choice choice points to choose among alternative continuations of the control flow or loops.

## 6.3 Stream-oriented programming

We consider here a simple stream-oriented approach akin to dataflow programming and stream programming, where we conceive an application as  a graph where nodes are data processing blocks that may produce, consume, transform and route data items (tuples), and where the edges relate the output stream of a block with the input stream of another block. 

For the sake of simplicity we consider the following blocks only:
 * Producer: consumes no input and produces a stream of tuples.
 * Consumer: consumes a stream of tuples and produces nothing.
 * Transformer: consumes a stream and produces a new one, by transforming the incoming tuples.
 * Duplicator: consumes a stream and produces two copies of the stream.
 * Dispatcher: consumes a stream an produces two streams. Each input tuple is redirected into one of the two output streams according to some dispatching function.
 * Collector: consumes two streams and produces a stream that results by arbitrily intertwining the input streams.
 * Merger: consumes two streams s1 and s2 and produces a stream where each tuple from s1 is merged together with a tuple from s2 according to some function.

As an example we consider a scenario where data produced by a set of sensors needs to be presented on some displays. Graphically the dataflow has the following structure:

```
 sensor1 --\
           |
          merger --> duplicator --> aggregator --> display1
           |             |
 sensor2 --/             \--> display2
```

Sensors are redundant and are meant to measure the same value. Redundancy is used to ensure better quality of the measurement. The data produced by the sensors flows into a merger that computes the average value for each timestamp. The stream of data produced by the merger is then duplicated to two displays. One gets the data as it is, the other get easier-to-digest data: to avoid overflowing the display with too much information, a transformer block takes care of consuming the stream and producing a new one where long sequences of values are compressed into a single (averaged) value.

A dataflow can be easily implented as a pSpace application. In the approach we sketch we start creating a space `stream(X,Y)` for every edge `X->Y` connnecting block `X` with block `Y`. Every block `X` is then programmed as explained below.

For a producer A and the unique flow A->X the code of the producer is

```
for {
    // produce the data
    stream(A,X).Put(data)
}
```
 
For a consumer A and the unique flow X->A the code of the consumer is

```
for {
    data,_ := stream(X,A).Get(template)
    // do something with the data
}
```

For a transformer A and the unique flows X->A , A->Y the code of the transformer based on a simple piece-wise transformation function f is 

```
for {
    stream(X,A).Get(&data)
    stream(A,Y).Put(f(data))
}
```

For a duplicator A and the unique flows X->A , A->Y , A->Z the code is

```
for {
    data,_ := stream(A,X).Get(template)
    stream(A,Y).Put(data)
    stream(A,Z).Put(data)
}
```

For a dispatcher A and the unique flows X->A , A->Y , A->Z the code is

```
for {
    t,_ := stream(X,A).Get(template)
    if (// some routing condition here) {
        stream(A,Y).Put(data)
    else {
        stream(A,Z).Put(data)
        }
}
```
 
A collector A with the unique flows X->A , Y->A , A->Z can be programmed as two parallel activities, each dealing with one of the input streams:

```
for {
    data,_ := stream(X,A).Get(template)
    stream(A,Z).Put(data)
}
```

and

```
for {
    data,_ := stream(Y,A).Get(template)
    stream(A,Z).Put(data)
}
```

For a merger A and the unique flows X->A , Y->A , A->Z the code is

```
for {
    dataX,_ := stream(X,A).Get(templateX)
    dataY,_ := stream(Y,A).Get(templateY)
    stream(A,Z).Put(merge(dataX,dataY))
}
```

The full code of the example can be found [here](https://github.com/pSpaces/goSpace-examples/tree/master/tutorial/dataflow-0).

## Summary

We have briefly described the following patterns in process-oriented distributed programming:
 * Client-server
 * Peer-to-peer
 * Actors

We have introduced two top-down modelling approaches
 * Task-oriented programming, with a simple form of workflows and a local concurrent implementation
 * Stream-oriented programming, with a basic set of data processing blocks with an implementation 