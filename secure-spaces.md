# 7. Secure Spaces

This chapter discusses security considerations when programming distributed applications with pSpaces. The chapter focuses on access control and introduces a simple acces control language for tuple spaces. The access control approach presented in this chapter is mainly based on the paper  [Advice from Belnap Policies](http://findit.dtu.dk/en/catalog/109845620). The chapter is illustrated with a scenario where Alice and her friends use and tuple space to store messages in inboxes and boards, and they want to rely on access control policies to provide some privacy and integrity guarantees. 
 
# 7.1 Access control

A tuple space is a shared data structure where several agents spread write and read information. In many applications one needs to regulate access to data, especially when users are not equally trusted. One key concern in such scenarios is *confidentiality*: data should be read only by a set of designated users and it should be hidden to the rest of the users. Confidentiality mechanisms can be used to provide *privacy* guarantees. A dual concern is *integrity*: only a set of designated users may be allowed to write certain kinds of data. Integrity mechanisms can be used to provide *trustworthiness* guarantees on data. Access control techniques are used to provide confidentiality and integrity guarantees by controlling *who* performs which kind of *action* on which *resource*.

We will consider a simple specification language for access control policies. The basic policy clauses are of the form

```
decision subject action template
```

where:

* `decision` is the decision to be taken: `permit` (to grant the operation) or `deny` (to forbid it),
* `subject` is the entity performing the action `action`
* `action` is a tuple space operation such as `Put`, `Get`, `Query`, etc.
* `template` is a template

As an running example we will consider a space `space` used to store messages among Alice and her friends. Messages are of the form

```
(place, to, from, messages)
```

where `place` can be either the public `board` or the private `inbox` of user `to`, and `from` is the author of the `message`. To keep things simple, we use strings for all fields.

To grant `Alice` access to read messages in her inbox we can use the clauses

```
permit Alice Query ("inbox", "Alice", string, string)
permit Alice QueryP ("inbox", "Alice", string, string)
permit Alice QueryAll ("inbox", "Alice", string, string)
```

Operations by users different from Alice or done by Alice but where the action or the template are different do not match the above clauses. For example, if Alice tries to perform operation

```
Query("inbox","Alice","Bob",string)
```

none of the clauses applies, not even the first one since the templates `("inbox","Alice","Bob",string)` and `("inbox", "Alice", string, string)` are different. The formal semantics of this simple language and the clauses will be defined later.

With similar clauses we can grant Alice the right to remove messages.

On the other hand, we can explicitly deny other users access to messages in Alice's inbox with the clauses like:  

```
deny Bob Query ("inbox", "Alice", string, string)
deny Bob QueryP ("inbox", "Alice", string, string)
deny Bob QueryAll ("inbox", "Alice", string, string)
...
```

# 7.2 Partial information and conflicts

Consider the policy defined the following clauses

```
permit Alice Query ("inbox", "Alice", string, string)
permit Alice QueryP ("inbox", "Alice", string, string)
permit Alice QueryAll ("inbox", "Alice", string, string)
deny Bob Query ("inbox", "Alice", string, string)
deny Bob QueryP ("inbox", "Alice", string, string)
deny Bob QueryAll ("inbox", "Alice", string, string)
```

and suppose that Charlie wants to read from Alice's inbox with a query

```
Query("inbox","Alice",string,string)
```

The policy does not provide any explicit clause to permit or deny the action. There are several alternative approaches one could adopt here:
* adopt a *restrictive* approach and deny all operations that are not explicitly allowed
* adopt  *permissive* approach and permit all operations that are not explicitly denied
* admit that not enough information is available introduce a new decision `maybe`, hoping that some other policy will resolve the question.

Our simple policy language allows us to adopt the three appoaches by using a rich set of operations to combine clauses and policies. Combining policies, however, can also lead to contradicting decisions. Consider for example the following clause, introduced by Alice to let Charlie read messages from her board:

```
permit Charlie Query ("board", Alice, string, string)
```

and this other clause, introduced by the system administrator to ban `Charlie` from the system:

```
deny Charlie to Query ("board", Alice, string, string)
```

The two clauses suggest contradictory decisions on the same set of operations. To deal with such cases we introduce a fourth decision that we call `conflict`.
 
# 7.3 Combining policies

All in all, the four access control decisions we have seen can be represented as a partial order of *strictness* (from denial to permission):

```
     permit
     /    \
maybe     conflict
     \    /
      deny
```

or as a partial order of *information* (from no information to "too much" information):

```
    conflict
     /    \
permit     deny
     \    /
      maybe
```

The two partial orders provide us with four well-defined operations that can be used to combine the access control decisions, namely the least and greatest upper bounds of each of the partial orders. We call the least upper and greatest lower bound operators of the strictness partial order, `or` and `and`, respectively. Notice that we can identify `permit` and `deny` with the logical values `true` and `false`, respectively.

The `and` operation is useful when we want to permit an operation only if a set of policies agrees on that. The operation is defined as follows:

```
      and | conflict | permit   | deny     | maybe 
-----------------------------------------------------
 conflict | conflict | conflict | deny     | deny     
 permit   | conflict | permit   | deny     | maybe  
 deny     | deny     | deny     | deny     | deny  
 maybe    | deny     | maybe    | deny     | maybe  
```


On the other hand, we call the least upper and greatest lower bound operators of the information partial order, `+` and `-`, respectively. Operation `+` is pretty useful as it allows us to combine the different decisions on the same action in a very reasonable way, for example by collecting the opinon of a set of clauses on a certain operation and putting all that information together. Let us see the `plus` operation in detail:

```
        + | conflict | permit   | deny     | maybe 
-----------------------------------------------------
 conflict | conflict | conflict | conflict | conflict      
 permit   | conflict | permit   | conflict | permit  
 deny     | conflict | conflict | deny     | deny  
 maybe    | conflict | permit   | deny     | maybe  
```

Additional binary operations can be of course defined but not all of them may make sense. 

# 7.4 Priorities

A very useful operator is one that will allow us to give priorities to clauses and policies so that we can easily resolve conflicts. Indeed it is sometimes convenient to structure policies in different layers of priority so that policies with higher priority are applied first and lower priority policies are applied only if the higher ones are conflicting or not informative enough. 

For this purpose one can introduce an operator `>` to specify priorities among policies. The `>` on decisions is defined by

```
        > | conflict | permit | deny   | maybe 
-----------------------------------------------------
 conflict | conflict | permit | deny   | maybe    
 permit   | permit   | permit | permit | permit  
 deny     | deny     | deny   | deny   | deny  
 maybe    | conflict | permit | deny   | maybe   
```

Note that the operator goes to the second policy if the first one does not provide decision which is either `permit` and `deny`. This measn that the second policy may also resolve `conflict` decisions.

We can instead consider `conflict` decisions as unresolvable by introducig variants of `>` like `>>` defined by as follows:


```
       >> | conflict | permit    | deny    | maybe 
-----------------------------------------------------
 conflict | conflict | conflict | conflict | conflict   
 permit   | permit   | permit   | permit   | permit  
 deny     | deny     | deny     | deny     | deny  
 maybe    | conflict | permit   | deny     | maybe   
```


# 7.5 A simple access control policy language

We are now ready to introduce a simple accces control policy language, starting from its syntax:

```
Policy ::= Decision | Clause | Policy Operator Policy 
Decision ::=  permit | deny | maybe | conflict
Clause ::=  Decision subject Action template
Operator ::= and | or | + | - | > | >> | ...
Action ::= Query | QueryP | QueryAll | ...
```

where `subject` are the credentials of the process willing to perform the operation and `template` is a tuple template.

Given a subject `S`, willing to perform the operation `A(T)`,  and a policy `P`, the decision to be taken according to the policy, denoted `check(P,S,A,T)`, is defined recursively by

```
check(D,       S, A, T) = D
check(D S A T, S, A, T) = D
check(P1 O P2, S, A, T) = check(P1 , S, A, T) O check(P2 , S, A, T) 
check(P,       S, A, T) = maybe   otherwise
```

## 7.6 Example policies

The first clauses that we saw at the beginning of this chapter can be combined in a policy `user-policy` by combining them with the `+` operator:

```
permit Alice Query ("inbox", "Alice", string, string)
+ permit Alice QueryP ("inbox", "Alice", string, string)
+ permit Alice QueryAll ("inbox", "Alice", string, string)
+ deny Bob Query ("inbox", "Alice", string, string)
+ deny Bob QueryP ("inbox", "Alice", string, string)
+ deny Bob QueryAll ("inbox", "Alice", string, string)
...
```

We can now adopt the restrictive approach to partial information and conflicts using priority operators, as in :

```
user-policy 
>
deny

```

or the permissive approach with: 

```
user-policy
>
permit

```

We could have also the rules that ban certain users as a policy `admin-policy` composed for example by:

```
deny Mallory Query ("board", "Alice", string, string)
+ deny Mallory QueryP ("board", "Alice", string, string)
...
```

and we can combine all policies together with

```
admin-policy >> user-policy > deny
```

## 7.6 Access control in pSpaces

Access control is currently not supported by any of the pSpace libraries (except for an experimental branch).

We can however, implement application-level access control mechanisms on top of pSpaces by building on the [remote procedure call](https://github.com/pSpaces/Programming-with-Spaces/blob/master/tutorial-distributed-programming.md#38-a-coordination-pattern-remote-procedure-call) pattern that we saw in a previous chapter.

The basic idea is the following:
* The data to be protected, messages in our example, is stored in a private tuple space `space`, hosted by a server and not accessible through the network.
* Clients don't have direct access to `space` and cannot perform tuple space operations on it. Instead, they send requests to the server, with the operations they want to execute. For example, Alice can read from her inbox with something like

```
server.Put("Alice", "QueryP", new Template("board", "Alice", string, string))
```
* The server listens for requests and checks whether they can be performed, informing the client accordingly.
* The core of the access control is the `check` function described above, which can be implemented as a recursive function that exploits some representation of the policy, typically as an abstract syntax tree.

A complete example, which implements the simple access control language discussed in this chapter can be found [here](https://github.com/pSpaces/goSpace-examples/blob/master/tutorial/access-control-1/main.go)

Some other considerations are:
* One may want to use user credentials that are more sophisticated than bare strings.
* Clients still need to talk to the server on public tuple spaces that would need some form of access control, to avoid malicious users to see requests and responses (and credentials) of other users, remove them, or act on their behalf:
 * Privacy of the *content* of the requests and servers can be obtained by encrypting requests that only the server can decrypt (e.g. using public key cryptography). Of course, malicious users would still be able to observe *presence* of requests and responses.
 * Malicous removal of requests cannot be prevented without built-in acces control. 
 * Denial-of-service attacks caused by a malicious user sending unauthorized requests (e.g. by replicating an observed request again and again) can be mitigated with techniques based on unique request ids that the malicous user would not be able to predict.

## Summary 

We have seen a simple approach to access control in tuple spaces based on:
* Simple clauses that allow to specify *who* can do *what* on which *kind* of tuple.
* Decisions values that allow to `permit` or `deny` operations or represent uncertainty and conflicts.
* Operators to combine policies and their decisions, for example to reach agreements or to establish priorities.
* An implementation scheme for application-level access control.

## Reading Suggestions
[Hankin, C., Nielson, F., and Nielson, H. R. (2009). Advice from belnap policies. In Proceedings of the 22nd IEEE Computer Security Foundations Symposium, CSF 2009, Port Jefferson, New York, USA, July 8-10, 2009, pages 234–247. IEEE Computer Society](http://findit.dtu.dk/en/catalog/10984562)