## Main lectures

1. Programming with spaces
 * Preparation: get familiar with [pSpaces](https://github.com/pSpaces/) and read [tutorial 01](https://github.com/pSpaces/Programming-with-Spaces/blob/master/tutorial-tuple-spaces.md).
 * Content: [tutorial 01]( https://github.com/pSpaces/Programming-with-Spaces/blob/master/tutorial-tuple-spaces.md).
 * Exercises: Exercises 1.1-1.5 from the [exercise page](exercises.md).

2. Concurrent programming
 * Preparation: read [tutorial 02](https://github.com/pSpaces/Programming-with-Spaces/blob/master/tutorial-concurrent-programming.md).
 * Content: [tutorial 02](https://github.com/pSpaces/Programming-with-Spaces/blob/master/tutorial-concurrent-programming.md).
 * Exercises: Exercises: 2.1-2.3 from the [exercise page](exercises.md).
 
3. Semantics of pSpace program
 * Preparation: read the [formal semantics for a light version of pSpaces](https://github.com/pSpaces/Programming-with-Spaces/blob/master/semantics-light.md)  page.
 * Content: the [formal semantics for a light version of pSpaces](https://github.com/pSpaces/Programming-with-Spaces/blob/master/semantics-light.md)  page.
 * Exercises: Exercises 3.1 and 3.2 in [exercise page](exercises.md).
 
4. Modelling and analysing pSpaces programs with Spin
 * Preparation: read about [how to use the Spin model checker to model and verify pSpace applications](https://github.com/pSpaces/Programming-with-Spaces/blob/master/Promela/promela.md).
 * Content: brief Spin tutorial and notes on [how to use the Spin model checker to model and verify pSpace applications](https://github.com/pSpaces/Programming-with-Spaces/blob/master/Promela/promela.md).
 * Exercises: Exercise 4.1-4.3 in [exercise page](exercises.md).

5. Distributed programming
 * Preparation: read [tutorial 03](https://github.com/pSpaces/Programming-with-Spaces/blob/master/tutorial-distributed-programming.md).
 * Content: [tutorial 03](https://github.com/pSpaces/Programming-with-Spaces/blob/master/tutorial-distributed-programming.md).
 * Exercises: Exercises 5.1 and 5.2 in [exercise page](exercises.md).

6. Coordination models
 * Preparation: read these [notes on coordination models](coordination-models.md).
 * Content: [notes on coordination models](https://gitlab.gbar.dtu.dk/02148/home/blob/master/coordination-models.md).
 * Exercises: Exercise 6.1 in [exercise page](exercises.md).
 
7. Secure Spaces
 * Preparation: read these [notes on secure tuple spaces](secure-spaces.md).
 * Content: [notes on secure tuple spaces](secure-spaces.md).
 * Exercises: Exercise 7.1 in [exercise page](exercises.md).

8. Interaction-oriented programming
 * Preparation: read these [notes on interaction-oriented programming](protocols.md).
 * Content: [notes on interaction-oriented programming](protocols.md).
 * Exercises: work on the project.

## Guest lectures
* [Thomas Hildebrandt](http://www.itu.dk/~hilde/) (IT University of Copenhagen), "declarative process modelling with DCR graphs" [[DCR tool]](http://dcrgraphs.com/)
* [Maaerten Faddegon](http://maartenfaddegon.nl/) (Motorola Solutions Denmark), "challenges in concurrent programming"